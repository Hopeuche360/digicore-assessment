public class StringAddition {
    public static void main(String[] args) {
        String number1 = "342";
        String number2 = "10";
        System.out.println(addStrings(number1,number2));
    }
    public static String addStrings(String firstvalue, String secondValue) {
        StringBuilder sb = new StringBuilder();
        int carry = 0;

        int i = firstvalue.length() - 1;
        int j = secondValue.length() - 1;
        while (i > -1 || j > -1) {
            int sum = carry + (i < 0 ? 0 : firstvalue.charAt(i--) - 48);
            sum += j < 0 ? 0 : secondValue.charAt(j--) - 48;
            sb.append(sum % 10);
            carry = sum / 10;
        }
        return sb.append(carry == 1 ? "1" : "").reverse().toString();
    }

}