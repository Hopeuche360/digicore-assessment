package com.digicoreassessment.challenge3.config.security;

import com.digicoreassessment.challenge3.config.UserDetailsImplService;
import com.digicoreassessment.challenge3.models.AccountUser;
import com.digicoreassessment.challenge3.models.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.util.HashMap;
import java.util.List;

@Configuration
@EnableWebSecurity
public class SecurityConfigurer extends WebSecurityConfigurerAdapter {


    private UserDetailsImplService userDetailsImplService;
    private JwtRequestFilter jwtRequestFilter;

    @Autowired
    public void setSecurityConfigurer(@Lazy UserDetailsImplService userDetailsImplService, @Lazy JwtRequestFilter jwtRequestFilter) {
        this.userDetailsImplService = userDetailsImplService;
        this.jwtRequestFilter = jwtRequestFilter;
    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(daoAuthenticationProvider());
    }

    @Bean
    public PasswordEncoder passwordEncoder(){

        return new BCryptPasswordEncoder(12);
    }


    @Bean
    public HashMap<String, AccountUser> userAccountNameToUserStore() {
        return new HashMap<>();
    }

    @Bean
    public HashMap<String, AccountUser> userTransactionDb() {
        return new HashMap<>();
    }

    @Bean
    public HashMap<String, String> acctNoToAcctNameStore() {
        return new HashMap<>();
    }

    @Bean
    public HashMap<String, AccountUser> acctNoToUserStore() {
        return new HashMap<>();
    }

    @Bean
    HashMap<String, List<Transaction>> transactionDb(){
        return new HashMap<>();
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/create_account", "/login").permitAll()
                .anyRequest().authenticated()
                .and().sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    protected DaoAuthenticationProvider daoAuthenticationProvider(){
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setUserDetailsService(userDetailsImplService);
        provider.setPasswordEncoder(passwordEncoder());
        return provider;
    }
}
