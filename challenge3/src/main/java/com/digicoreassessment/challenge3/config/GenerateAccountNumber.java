package com.digicoreassessment.challenge3.config;

import java.util.Random;

public class GenerateAccountNumber {
    public static String generateNumber() {
        String[] numbers = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
        Random random = new Random();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < numbers.length; i++) {
            int value = random.nextInt(numbers.length);
            stringBuilder.append(numbers[value]);
        }
        return stringBuilder.toString();
    }
}
