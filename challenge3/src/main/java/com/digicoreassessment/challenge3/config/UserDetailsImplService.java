package com.digicoreassessment.challenge3.config;

import com.digicoreassessment.challenge3.models.AccountUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class UserDetailsImplService implements UserDetailsService {

    @Autowired
    private HashMap<String, AccountUser> userAccountNameToUserStore;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        return UserDetailsImpl.buildUserDetail(userAccountNameToUserStore.get(s));
    }

}
