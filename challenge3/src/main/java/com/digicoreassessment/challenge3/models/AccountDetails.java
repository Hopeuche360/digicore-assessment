package com.digicoreassessment.challenge3.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountDetails {
    private String accountName;
    private String accountNumber;
    private Double accountBalance;

    public AccountDetails(String accountName, String accountNumber, Double accountBalance) {
        this.accountName = accountName;
        this.accountNumber = accountNumber;
        this.accountBalance = accountBalance;
    }
}
