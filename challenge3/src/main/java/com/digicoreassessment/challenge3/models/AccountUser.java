package com.digicoreassessment.challenge3.models;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AccountUser {
    private String accountName;
    private String password;
    private AccountDetails accountDetails;

    public AccountUser(String accountName, String password, AccountDetails accountDetails) {
        this.accountName = accountName;
        this.password = password;
        this.accountDetails = accountDetails;
    }
}
