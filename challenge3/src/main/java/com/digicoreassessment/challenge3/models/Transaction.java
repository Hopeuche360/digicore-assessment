package com.digicoreassessment.challenge3.models;

import com.digicoreassessment.challenge3.enums.TransactionType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class Transaction {
    private TransactionType transactionType;
    private LocalDateTime transactionDate;
    private Double transactionAmount;
    private Double accountBalance;

    public Transaction(TransactionType transactionType, LocalDateTime transactionDate, Double transactionAmount, Double accountBalance) {
        this.transactionType = transactionType;
        this.transactionDate = transactionDate;
        this.transactionAmount = transactionAmount;
        this.accountBalance = accountBalance;
    }
}
