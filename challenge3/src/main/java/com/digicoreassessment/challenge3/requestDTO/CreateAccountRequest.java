package com.digicoreassessment.challenge3.requestDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class CreateAccountRequest {
    private String accountName;
    private String accountPassword;
    private Double initialDeposit;
}
