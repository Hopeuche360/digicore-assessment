package com.digicoreassessment.challenge3.requestDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WithdrawalRequest {
    private String accountNumber;
    private String accountPassword;
    private Double withdrawnAmount;
}
