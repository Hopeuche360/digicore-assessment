package com.digicoreassessment.challenge3.requestDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginRequest {
    private String accountNumber;
    private String password;
}
