package com.digicoreassessment.challenge3.controllers;

import com.digicoreassessment.challenge3.requestDTO.CreateAccountRequest;
import com.digicoreassessment.challenge3.requestDTO.DepositRequest;
import com.digicoreassessment.challenge3.requestDTO.LoginRequest;
import com.digicoreassessment.challenge3.requestDTO.WithdrawalRequest;
import com.digicoreassessment.challenge3.responseDTO.AccountInfoResponse;
import com.digicoreassessment.challenge3.responseDTO.CreateAccountResponse;
import com.digicoreassessment.challenge3.responseDTO.DepositResponse;
import com.digicoreassessment.challenge3.responseDTO.WithdrawalResponse;
import com.digicoreassessment.challenge3.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AccountController {

    UserService userService;

    @Autowired
    public AccountController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/account_info/{accountNo}")
    public ResponseEntity<AccountInfoResponse> getAccountInfo(@PathVariable ("accountNo") String acctNo){
        return userService.getAccountInfo(acctNo);
    }

    @GetMapping("/account_statement/{accountNo}")
    public ResponseEntity<?> getAccountTransactionInfo(@PathVariable ("accountNo") String acctNo){
        return userService.getAccountTransactionInfo(acctNo);
    }

    @PostMapping("/deposit")
    public ResponseEntity<DepositResponse> deposit(@RequestBody DepositRequest depositRequest){
        return userService.doDeposit(depositRequest);
    }

    @PostMapping("/withdrawal")
    public ResponseEntity<WithdrawalResponse> withdraw(@RequestBody WithdrawalRequest withdrawalRequest){
        return userService.doWithdrawal(withdrawalRequest);
    }

    @PostMapping("/create_account")
    public ResponseEntity<CreateAccountResponse> createAccount(@RequestBody CreateAccountRequest userRequest){
       return userService.createUser(userRequest);
    }


    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody LoginRequest userRequest) throws Exception {
        return userService.login(userRequest);
    }
}
