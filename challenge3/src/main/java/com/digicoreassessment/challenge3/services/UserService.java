package com.digicoreassessment.challenge3.services;

import com.digicoreassessment.challenge3.config.GenerateAccountNumber;
import com.digicoreassessment.challenge3.config.security.JwtTokenProvider;
import com.digicoreassessment.challenge3.config.UserDetailsImplService;
import com.digicoreassessment.challenge3.enums.TransactionType;
import com.digicoreassessment.challenge3.models.AccountDetails;
import com.digicoreassessment.challenge3.models.AccountUser;
import com.digicoreassessment.challenge3.models.Transaction;
import com.digicoreassessment.challenge3.requestDTO.CreateAccountRequest;
import com.digicoreassessment.challenge3.requestDTO.DepositRequest;
import com.digicoreassessment.challenge3.requestDTO.LoginRequest;
import com.digicoreassessment.challenge3.requestDTO.WithdrawalRequest;
import com.digicoreassessment.challenge3.responseDTO.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class UserService {

    private HashMap<String, AccountUser> acctNoToUserStore;

    private HashMap<String, String> acctNoToAcctNameStore;

    private HashMap<String, AccountUser> userAccountNameToUserStore;

    private AuthenticationManager authenticationManager;

    private UserDetailsImplService userDetailsService;

    private JwtTokenProvider jwtTokenProvider;

    private PasswordEncoder passwordEncoder;

    private HashMap<String, List<Transaction>> transactionDb;

    @Autowired
    public UserService(HashMap<String, AccountUser> acctNoToUserStore, HashMap<String, String> acctNoToAcctNameStore, HashMap<String, AccountUser> userAccountNameToUserStore, AuthenticationManager authenticationManager, UserDetailsImplService userDetailsService, JwtTokenProvider jwtTokenProvider, PasswordEncoder passwordEncoder, HashMap<String, List<Transaction>> transactionDb) {
        this.acctNoToUserStore = acctNoToUserStore;
        this.acctNoToAcctNameStore = acctNoToAcctNameStore;
        this.userAccountNameToUserStore = userAccountNameToUserStore;
        this.authenticationManager = authenticationManager;
        this.userDetailsService = userDetailsService;
        this.jwtTokenProvider = jwtTokenProvider;
        this.passwordEncoder = passwordEncoder;
        this.transactionDb = transactionDb;
    }


    public ResponseEntity<CreateAccountResponse> createUser(CreateAccountRequest userRequest){
        if(userRequest.getInitialDeposit() < 500){

            CreateAccountResponse userResponse = new CreateAccountResponse(false,
                    "User Account successfully created", "Initial deposit of "+userRequest.getInitialDeposit()
                            +" was rejected, Initial deposit minimum is #500");
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(userResponse);

        }else if(acctNoToAcctNameStore.containsValue(userRequest.getAccountName())){

            CreateAccountResponse userResponse = new CreateAccountResponse(false,
                    "User Account successfully created", "User Account was not created as a User with account name: " +userRequest.getAccountName()
                            +" already exist");
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(userResponse);

        }else {

            String acctNo = GenerateAccountNumber.generateNumber();
            while (acctNoToUserStore.containsKey(acctNo)) {
                acctNo = GenerateAccountNumber.generateNumber();
            }

            AccountDetails account = new AccountDetails(acctNo,userRequest.getAccountName(), userRequest.getInitialDeposit());
            AccountUser user = new AccountUser(userRequest.getAccountName(),
                    passwordEncoder.encode(userRequest.getAccountPassword()), account);

            user.setAccountDetails(account);
            Transaction transaction = new Transaction(TransactionType.DEPOSIT, LocalDateTime.now(),
                    userRequest.getInitialDeposit(),user.getAccountDetails().getAccountBalance());
            List<Transaction> userTransaction;
            if (transactionDb.containsKey(acctNo)) {
                userTransaction = transactionDb.get(acctNo);
            }else {
                userTransaction = new ArrayList<>();

            }
            userTransaction.add(transaction);

            acctNoToUserStore.put(acctNo, user);
            acctNoToAcctNameStore.put(acctNo, user.getAccountName());
            transactionDb.put(acctNo, userTransaction);
            userAccountNameToUserStore.put(userRequest.getAccountName(), user);

            CreateAccountResponse userResponse = new CreateAccountResponse(true,
                    "User Account successfully created", acctNo);
            return ResponseEntity.status(HttpStatus.OK).body(userResponse);
        }
    }

    public ResponseEntity<String> login(LoginRequest userRequest) throws Exception {
        String acctNo = userRequest.getAccountNumber();
        String username = acctNoToAcctNameStore.get(acctNo);
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(username,
                            userRequest.getPassword())
            );
        }
        catch (BadCredentialsException e) {
            throw new Exception("Incorrect username or password", e);
        }

        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(username);

        final String jwt = jwtTokenProvider.generateToken(userDetails);

        return ResponseEntity.ok(jwt);
    }

    public ResponseEntity<AccountInfoResponse> getAccountInfo(String acctNo){
        if(!acctNoToUserStore.containsKey(acctNo)){
            AccountInfoResponse response = new AccountInfoResponse(false,"Account does not exist");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
        AccountUser user = acctNoToUserStore.get(acctNo);
        AccountInfoResponse response = new AccountInfoResponse(true,"Account statement is as below:",
                user.getAccountDetails());
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    public ResponseEntity<?> getAccountTransactionInfo(String acctNo){
        if(!transactionDb.containsKey(acctNo)){

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Account does not exist");
        }
        List<Transaction> transactionList = transactionDb.get(acctNo);
        List<AccountStatementResponse> transactionInfoList = new ArrayList<>();
        for (Transaction transaction : transactionList) {
            AccountStatementResponse accountStatementResponse = new AccountStatementResponse(
                    transaction.getTransactionDate(), transaction.getTransactionType(), "Successful",
                    transaction.getTransactionAmount(), transaction.getAccountBalance()
            );
            transactionInfoList.add(accountStatementResponse);
        }

        return ResponseEntity.status(HttpStatus.OK).body(transactionInfoList);
    }

    public ResponseEntity<DepositResponse> doDeposit(DepositRequest depositRequest){
        String acctNo = depositRequest.getAccountNumber();
        AccountUser user = acctNoToUserStore.get(depositRequest.getAccountNumber());
        if(!acctNoToUserStore.containsKey(acctNo)){
            DepositResponse depositResponse = new DepositResponse(false, "Account does not exist");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(depositResponse);
        }
        if(depositRequest.getAmount() > 1000000 || depositRequest.getAmount() < 1){
            DepositResponse depositResponse = new DepositResponse(false, "Deposit exceeds or is lower than limit; " +
                    "Maximum deposit is #1,000,000 and minimum is #1");
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(depositResponse);
        }
        user.getAccountDetails().setAccountBalance(user.getAccountDetails().getAccountBalance() + depositRequest.getAmount());
        acctNoToUserStore.put(acctNo, user);
        userAccountNameToUserStore.put(user.getAccountName(), user);

        Transaction transaction = new Transaction(TransactionType.DEPOSIT, LocalDateTime.now(),
                depositRequest.getAmount(),user.getAccountDetails().getAccountBalance());

        List<Transaction> userTransaction = transactionDb.get(acctNo);
        userTransaction.add(transaction);
        transactionDb.put(acctNo, userTransaction);

        DepositResponse depositResponse = new DepositResponse(true, "Deposit of #" +depositRequest.getAmount()
        +" was successful");
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(depositResponse);
    }

    public ResponseEntity<WithdrawalResponse> doWithdrawal(WithdrawalRequest withdrawRequest){
        String acctNo = withdrawRequest.getAccountNumber();
        AccountUser user = acctNoToUserStore.get(withdrawRequest.getAccountNumber());
        if(!acctNoToUserStore.containsKey(acctNo)){
            WithdrawalResponse response = new WithdrawalResponse(false,"Account does not exist");
            return ResponseEntity.status(HttpStatus.OK).body(response);
        }
        if(withdrawRequest.getWithdrawnAmount() < 1){
            WithdrawalResponse response = new WithdrawalResponse(false,"Withdrawal of "+withdrawRequest.getWithdrawnAmount()
                    +" is below withdraw limit");
            return ResponseEntity.status(HttpStatus.OK).body(response);
        }
        if(user.getAccountDetails().getAccountBalance() - withdrawRequest.getWithdrawnAmount() < 500){
            WithdrawalResponse response = new WithdrawalResponse(false,"Insufficient balance, as you must have a " +
                    "remainder balance of #500");
            return ResponseEntity.status(HttpStatus.OK).body(response);
        }

        user.getAccountDetails().setAccountBalance(user.getAccountDetails().getAccountBalance() - withdrawRequest.getWithdrawnAmount());
        acctNoToUserStore.put(acctNo, user);
        userAccountNameToUserStore.put(user.getAccountName(), user);

        Transaction transaction = new Transaction(TransactionType.WITHDRAWAL,LocalDateTime.now(),
                withdrawRequest.getWithdrawnAmount(),user.getAccountDetails().getAccountBalance());

        List<Transaction> userTransaction = transactionDb.get(acctNo);
        userTransaction.add(transaction);
        transactionDb.put(acctNo, userTransaction);
        WithdrawalResponse response = new WithdrawalResponse(true,"Withdrawal of " +withdrawRequest.getWithdrawnAmount()
        +" was successful; available balance is #" +user.getAccountDetails().getAccountBalance());
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
}
