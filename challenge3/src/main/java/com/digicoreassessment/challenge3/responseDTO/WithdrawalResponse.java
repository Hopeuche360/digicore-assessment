package com.digicoreassessment.challenge3.responseDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WithdrawalResponse {
    private boolean status;
    private String message;

    public WithdrawalResponse(boolean status, String message) {
    }
}
