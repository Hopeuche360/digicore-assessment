package com.digicoreassessment.challenge3.responseDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateAccountResponse {
    private String accountNumber;
    private boolean status;
    private String message;

    public CreateAccountResponse(boolean status, String message, String accountNumber) {
    }
}
