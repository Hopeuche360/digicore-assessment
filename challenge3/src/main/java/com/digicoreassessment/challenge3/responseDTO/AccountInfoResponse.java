package com.digicoreassessment.challenge3.responseDTO;

import com.digicoreassessment.challenge3.models.AccountDetails;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class AccountInfoResponse {
    private boolean status;
    private String message;
    private AccountDetails account;

    public AccountInfoResponse(boolean status, String message) {
        this.status = status;
        this.message = message;
    }
}
