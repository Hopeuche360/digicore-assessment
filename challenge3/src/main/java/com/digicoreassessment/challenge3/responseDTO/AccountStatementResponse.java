package com.digicoreassessment.challenge3.responseDTO;

import com.digicoreassessment.challenge3.enums.TransactionType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class AccountStatementResponse {
    private LocalDateTime transactionDate;
    private TransactionType transactionType;
    private String narration;
    private Double amount;
    private Double accountBalance;

    public AccountStatementResponse(LocalDateTime transactionDate, TransactionType transactionType, String narration, Double amount, Double accountBalance) {
        this.transactionDate = transactionDate;
        this.transactionType = transactionType;
        this.narration = narration;
        this.amount = amount;
        this.accountBalance = accountBalance;
    }
}
