package com.digicoreassessment.challenge3.responseDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginResponse {
    private boolean status;
    private String accessToken;
}
