package com.digicoreassessment.challenge3.responseDTO;

import lombok.Getter;

@Getter
public class DepositResponse {
    private boolean status;
    private String message;

    public DepositResponse(boolean status, String message) {
    }
}
