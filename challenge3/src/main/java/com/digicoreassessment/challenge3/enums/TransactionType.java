package com.digicoreassessment.challenge3.enums;

public enum TransactionType {
    DEPOSIT, WITHDRAWAL
}
