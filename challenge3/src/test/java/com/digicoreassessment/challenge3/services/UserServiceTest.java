package com.digicoreassessment.challenge3.services;
;
import com.digicoreassessment.challenge3.models.AccountUser;
import com.digicoreassessment.challenge3.requestDTO.CreateAccountRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class UserServiceTest {

    @Autowired
    private UserService userServices;

    @Qualifier("acctNoToUserStore")
    @Autowired
    private HashMap<String, AccountUser> datastore;

    @Test
    void createUser() {
        CreateAccountRequest request = new CreateAccountRequest("Papi",
                "1234", 5000d);
        userServices.createUser(request);
        assertEquals(datastore.size(), 1);
    }


}